# gatsby-gitlab-pages

*ENHANCED* example Gatsby site using GitLab Pages on v2 release ✨🔄🖼

https://rdela.gitlab.io/gatsby-gitlab-pages/

Kick off your project with this *ENHANCED* default boilerplate with GitLab Pages
support and 200% more rotating images, on Gatsby v2.

## Forked from pages/gatsby

https://gitlab.com/pages/gatsby

https://pages.gitlab.io/gatsby

## …Which was forked from gatsbyjs/gatsby-starter-default

[github.com/gatsbyjs/gatsby/tree/master/starters/default — The default Gatsby starter](https://github.com/gatsbyjs/gatsby/tree/master/starters/default)

For an overview of default Gatsby starter project structure please refer to the
[Gatsby documentation - Building with Components](https://www.gatsbyjs.org/docs/building-with-components/)

## License

https://docs.npmjs.com/files/package.json#license

> You should specify a license for your package so that people know how they are permitted to use it, and any restrictions you're placing on it.

> If you are using a license that hasn't been assigned an SPDX identifier, or if you are using a custom license, use a string value like this one:

> `{ "license" : "SEE LICENSE IN <filename>" }`

[OpenBSD license](https://en.wikipedia.org/wiki/ISC_license#OpenBSD_license)
in [license.txt](license.txt)

> Consider also setting "private": true to prevent accidental publication.

[package.json#L6-7](package.json#L6-7)

```json
{
  "license": "SEE LICENSE IN license.txt",
  "private": true
}
```

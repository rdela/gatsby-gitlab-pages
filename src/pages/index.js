import React from 'react'
import Link from 'gatsby-link'
import {Helmet} from 'react-helmet'

import Layout from '../components/layout'
import Image from '../components/astronaut-image'

const IndexPage = () => (
  <Layout>
    <Helmet
      title="Gatsby GitLab Pages"
      meta={[
        {
          name: 'description',
          content:
            'Kick off your next GitLab Pages project with this Gatsby starter.',
        },
        { name: 'keywords', content: 'gatsby, gitlab, pages, starter' },
      ]}
    />
    <div
      style={{
        maxWidth: '300px',
        margin: '63px auto',
        animation: 'astronaut-spin infinite 30s linear',
        animationDelay: '2s',
        height: '300px',
      }}
    >
      <Image />
    </div>
    <article>
      <h1>Welcome to Gatsby GitLab Pages</h1>
      <p>Now go build something gitterrific!</p>
      <p>
        <a href="https://gitlab.com/rdela/gatsby-gitlab-pages">
          Repo on GitLab
        </a>
      </p>
      <h3>
        <Link to="/about/">About Gatsby GitLab Pages Starter</Link>
      </h3>
    </article>
  </Layout>
)

export default IndexPage

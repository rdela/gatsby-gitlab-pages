import React from 'react'
import Link from 'gatsby-link'
import {Helmet} from 'react-helmet'

import Layout from '../components/layout'
import Image from '../components/logo-image'

const AboutPage = () => (
  <Layout>
    <Helmet
      title="About Gatsby GitLab Pages"
      meta={[
        {
          name: 'description',
          content: 'Background of GitLab Pages Gatsby starter.',
        },
        { name: 'keywords', content: 'gatsby, gitlab, pages' },
      ]}
    />
    <div
      style={{
        maxWidth: '160px',
        margin: '34px auto',
        animation: 'astronaut-spin infinite 30s linear',
        animationDelay: '2s',
        height: '160px',
      }}
    >
      <Image />
    </div>
    <article>
      <h1>About Gatsby GitLab Pages</h1>
      <h2>Example Gatsby starter site using GitLab Pages</h2>
      <p>
        <a href="https://gitlab.com/rdela/gatsby-gitlab-pages">
          Repo on GitLab
        </a>
      </p>
      <h3>
        <Link to="/">Go back to the homepage</Link>
      </h3>
    </article>
  </Layout>
)

export default AboutPage

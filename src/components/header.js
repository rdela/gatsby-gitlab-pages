import React from 'react'
import { Link } from 'gatsby'

import Image from '../components/logo-image'

const Header = () => (
  <header
    style={{
      background: '#e4b7f8',
      marginBottom: '1.45rem',
    }}
  >
    <nav
      style={{
        margin: '0 auto',
        maxWidth: 960,
        padding: '1.45rem 1.0875rem',
      }}
    >
      <h1
        style={{
          margin: 0,
          lineHeight: '64px',
          textAlign: 'center',
        }}
      >
        <Link
          to="/"
          style={{
            color: 'rebeccapurple',
            textDecoration: 'none',
          }}
        >
          <div
            style={{
              display: 'inline-block',
              padding: '0 8px',
              verticalAlign: 'bottom',
              width: '80px',
            }}
          >
            <Image />
          </div>{' '}
          GitLab&nbsp;Pages
        </Link>
      </h1>
    </nav>
  </header>
)

export default Header
